"""
Flask web site with vocabulary matching game
(identify vocabulary words that can be made 
from a scrambled string)
"""

import flask
import logging

# Our own modules
from letterbag import LetterBag
from vocab import Vocab
from jumble import jumbled
import config
import os

###
# Globals
###
app = flask.Flask(__name__)

CONFIG = config.configuration()
#app.secret_key = CONFIG.SECRET_KEY  # Should allow using session variables
app.secret_key = os.urandom(24)

#
# One shared 'Vocab' object, read-only after initialization,
# shared by all threads and instances.  Otherwise we would have to
# store it in the browser and transmit it on each request/response cycle,
# or else read it from the file on each request/responce cycle,
# neither of which would be suitable for responding keystroke by keystroke.

#get a list of vocab words
WORDS = Vocab(CONFIG.VOCAB)

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    """The main page of the application"""
    #turn the list of vocab words into a list
    #set vocab to be a global variable
    flask.g.vocab = WORDS.as_list()
    #create a variable passed from session to session for target_count
    flask.session["target_count"] = min(
        len(flask.g.vocab), CONFIG.SUCCESS_AT_COUNT)
    #jumble up all the vocab words into an anagram of targt_count strings
    flask.session["jumble"] = jumbled(
        flask.g.vocab, flask.session["target_count"])
    #create a list of words that have been matched
    flask.session["matches"] = []
    app.logger.debug("Session variables have been set")
    #the assert keyword lets us tests if a condition in our code is true
    #if it is not true, it will raise an AssertionError
    assert flask.session["matches"] == []
    assert flask.session["target_count"] > 0
    app.logger.debug("At least one seems to be set correctly")
    #display the vocab.html template
    return flask.render_template('vocab.html')

@app.route("/_countem")
def countem():
    #get what is passed into the query string in the text parameter
    text = flask.request.args.get("text", type=str)
    #get the length of the text
    length = len(text)
    #if check if the text in long enough
    #if it then rslt = "long_enough"
    #if it is not long enough then rslt = 
    #get the global variables for matches, jumble and in text
    #mtches = flask.session.get("matches", [])
    #jmble = flask.session["jumble"]
    #print(jmble)
    #need to test if text is in words
    #inWords = WORDS.has(text)

    #check if the text is in matches
    matches = flask.session.get("matches", [])
    jumble = flask.session["jumble"]
    in_matches = False
    if text in matches:
        in_matches = True
    #check if text is in jumble
    in_jumble = LetterBag(jumble).contains(text)
    in_words = WORDS.has(text)

    #create the JSON object
    rslt = {"long_enough": length >= 5, 
        "in_matches": in_matches, 
        "in_jumble": in_jumble, 
        "hasText": in_words}

    #return a json object that is passed to the front end in json
    return flask.jsonify(result=rslt)

@app.route("/keep_going")
def keep_going():
    """
    After initial use of index, we keep the same scrambled
    word and try to get more matches
    """
    flask.g.vocab = WORDS.as_list()
    return flask.render_template('vocab.html')


@app.route("/success")
def success():
    return flask.render_template('success.html')

#######################
# Form handler.
# CIS 322 note:
#   You'll need to change this to a
#   a JSON request handler
#######################


#@app.route("/_check", methods=["POST"])
@app.route("/_check")
def check():
    """
    User has submitted the form with a word ('attempt')
    that should be formed from the jumble and on the
    vocabulary list.  We respond depending on whether
    the word is on the vocab list (therefore correctly spelled),
    made only from the jumble letters, and not a word they
    already found.
    """
    app.logger.debug("Entering check")
    #get the request argument for text
    text = flask.request.args.get("text", type=str)
    # The data we need, from form and from cookie
    #get what the user put into the guessing form
    #text = flask.request.form["attempt"]
    #get what the jumple of letters they were given was
    jumble = flask.session["jumble"]
    #get the current matches, if none the list is empty
    matches = flask.session.get("matches", [])  # Default to empty list
    matches_count = len(matches)
    # Is it good?
    #check to see if the guess is in the jumbled letterbag they were given
    in_jumble = LetterBag(jumble).contains(text)
    #now check to see if it is the list of vocab words
    matched = WORDS.has(text)

    rslt = {"in_matches": (text in matches),
            "in_jumble": in_jumble,
            "matched": matched,
            "message": "",
            "reset": False,
            "won": False,
            "matches_count": matches_count,
            "match_to_add": None
    };
    print(rslt)

    # Respond appropriately
    #if it is a match to a vocab word and it is in the jumble given
    #and it is not a previous match
    if matched and in_jumble and not (text in matches):
        # Cool, they found a new word
        #add the match to matches
        matches.append(text)
        rslt["matches_count"] = len(matches)
        #update the session variable of matches
        flask.session["matches"] = matches
        rslt["reset"] = True
        rslt["match_to_add"] = text

    #if it is a previous match
    elif text in matches:
        #give them a flash message letting them know
        #flask.flash("You already found {}".format(text))
        rslt["message"] = "You already found {}".format(text)
    #if they did not use any of the letters given in the jumble, let them know
    elif not in_jumble:
        #flask.flash('"{}" can\'t be made from the letters {}'.format(text, jumble))
        rslt["message"] = '"{}" can\'t be made from the letters {}'.format(text, jumble)
    #if it is not a match
    elif not matched:
        #give them a flash message letting them no there was no match
        #flask.flash("{} isn't in the list of words".format(text))
        rslt["message"] = "{} isn't in the list of words".format(text)
    #this case should never be hit, if it is something went wrong and we will give an error
    else:
        app.logger.debug("This case shouldn't happen!")
        assert False  # Raises AssertionError

    # Choose page:  Solved enough, or keep going?
    #if we have solved for the number of desired matches go to the success page
    #route is /success
    if len(matches) >= flask.session["target_count"]:
        rslt["won"] = True
        rslt["message"] = "Vocabulary Anagram: Success!"
        #return flask.render_template("success.html"), 200
       #return flask.redirect(flask.url_for("success"))
       #return flask.render_template("success.html")
    #otherwise we want to keep going
    #eventually sends us back to vocab.html
    #else:
        #print("returns json")
        #return a json object that is passed to the front end in json
    return flask.jsonify(result=rslt)

###############
# AJAX request handlers
#   These return JSON, rather than rendering pages.
###############


@app.route("/_example")
def example():
    """
    Example ajax request handler
    """
    app.logger.debug("Got a JSON request")
    rslt = {"key": "value"}
    return flask.jsonify(result=rslt)


#################
# Functions used within the templates
#################

@app.template_filter('filt')
def format_filt(something):
    """
    Example of a filter that can be used within
    the Jinja2 code
    """
    return "Not what you asked for"

###################
#   Error handlers
###################


@app.errorhandler(404)
def error_404(e):
    app.logger.warning("++ 404 error: {}".format(e))
    return flask.render_template('404.html'), 404


@app.errorhandler(500)
def error_500(e):
    app.logger.warning("++ 500 error: {}".format(e))
    assert not True  # I want to invoke the debugger
    return flask.render_template('500.html'), 500


@app.errorhandler(403)
def error_403(e):
    app.logger.warning("++ 403 error: {}".format(e))
    return flask.render_template('403.html'), 403


####

if __name__ == "__main__":
    if CONFIG.DEBUG:
        app.debug = True
        app.logger.setLevel(logging.DEBUG)
        app.logger.info(
            "Opening for global access on port {}".format(CONFIG.PORT))
        app.run(port=CONFIG.PORT, host="0.0.0.0")
