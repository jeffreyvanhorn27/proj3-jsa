# README #

### Description ###
A project that uses jQuery and Ajax for an anagram game.

### Author and Contact Info ###
Jeffrey Van Horn
jeffreyv@uoregon.edu 

### Requirments ###
Docker

### Other Notes ###
If nosetests have difficulty running in Docker, specifically on Windows, use --exe flag when running docker exec